# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/29 12:29:11 by nmatushe          #+#    #+#              #
#    Updated: 2017/04/14 13:30:12 by nmatushe         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d

HEAD = includes/

VPATH = srcs:includes

FLAGS = -O3 -Wall -Wextra -Werror -I $(HEAD)

MLX = -lmlx -framework AppKit -framework OpenGl

SRCS = main_all.c									\
		ft_read.c									\
		raycasting.c									\
		ft_tools.c									\
		ft_pxl.c									\

BINS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(BINS)
	 make re -C libft/ fclean && make -C libft/
	gcc -o $(NAME) -I libft/includes $(BINS) $(FLAGS) -L. libft/libft.a $(MLX)

%.o: %.c
	gcc -I libft/includes $(FLAGS) -c -o $@ $<

clean:
	/bin/rm -f $(BINS) && make -C libft/ fclean

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all
