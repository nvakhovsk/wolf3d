/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 15:29:31 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/14 13:21:09 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include "libft.h"
# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include <time.h>

# define W 1000
# define H 1000

typedef struct	s_in
{
	void	*init;
	void	*win;
	void	*image;
}				t_in;

typedef struct	s_player
{
	double	x_position;
	double	y_position;
	double	dir_vect_x;
	double	dir_vect_y;
	double	cam_plane_x;
	double	cam_plane_y;
	double	time_cur_fr;
	double	time_prev_fr;
	double	move_speed;
	double	rot_speed;
}				t_player;

typedef struct	s_camera
{
	double cam_coord_x;
	double ray_pos_x;
	double ray_pos_y;
	double ray_dir_x;
	double ray_dir_y;
}				t_camera;

typedef struct	s_wolf
{
	t_in		*in;
	t_player	*player;
	t_camera	*camera;
	int			color;
	int			move_for_fl;
	int			move_back_fl;
	int			rot_left_fl;
	int			rot_right_fl;
	int			**map;
	int			w;
	int			h;
	int			line_heigh;
	int			draw_start;
	int			draw_stop;
	int			mapx;
	int			mapy;
	double		side_dist_x;
	double		side_dist_y;
	double		delta_dist_x;
	double		delta_dist_y;
	double		dist;
	int			xstep;
	int			ystep;
	int			dda_flag;
	int			side;
}				t_wolf;

typedef struct	s_brezen
{
	float		dx;
	float		dy;
	float		sx;
	float		sy;
	float		err0;
	float		err1;
}				t_brezen;

typedef struct	s_cmap
{
	float		x;
	float		y;
}				t_cmap;

void			ft_ariadne(t_wolf *wolf);
void			ft_read(int ***am, int *w, int *h);
void			ft_err(void);
void			ft_raycasting(t_wolf *wolf);
void			ft_brezenhaim(t_cmap d1, t_cmap d2, int color, t_in *in);
int				ft_exit(void *tmp);
int				ft_key_release(int key, t_wolf *wolf);
int				ft_key_press(int key, t_wolf *wolf);
void			ft_move(t_wolf *wolf, int flag);
void			ft_rotation(t_wolf *wolf, int flag);
void			ft_side_dist(t_wolf *wolf,int flag);
void			ft_ray_camera_init(t_wolf *wolf, int i);
void			ft_map_move(t_wolf *wolf);
void			ft_map_draw(t_wolf *wolf, int i);
void			ft_redraw(t_wolf *wolf);
void			ft_pxlimg(int x, int y, int rgb, t_in *in);
#endif
