/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 14:51:42 by nmatushe          #+#    #+#             */
/*   Updated: 2016/12/02 10:15:38 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list			*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*tlst;
	size_t	i;

	if (!(tlst = (t_list*)malloc(sizeof(t_list))))
		return (0);
	if (!content)
		tlst->content = 0;
	else
	{
		tlst->content = malloc(content_size);
		i = 0;
		while (i < content_size)
		{
			((char *)(tlst->content))[i] = ((char *)(content))[i];
			i++;
		}
	}
	if (content)
		tlst->content_size = content_size;
	else
		tlst->content_size = 0;
	tlst->next = 0;
	return (tlst);
}
