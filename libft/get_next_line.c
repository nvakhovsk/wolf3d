/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 17:27:49 by nmatushe          #+#    #+#             */
/*   Updated: 2017/03/04 10:43:23 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

int		findn(char *all)
{
	char	*temp;

	temp = all;
	while (all && *all)
	{
		if (*all == '\n')
			return (all - temp);
		all++;
	}
	return (-1);
}

char	*dup_all(char **all)
{
	int		l;
	char	*str;
	char	*tmp;

	if ((l = findn(*all)) >= 0)
	{
		str = ft_strndup(*all, l);
		tmp = *all;
		*all = ft_strdup(&(*all)[l + 1]);
		free(tmp);
	}
	else
	{
		str = ft_strdup(*all);
		free(*all);
		*all = NULL;
	}
	return (str);
}

int		get_next_line(const int fd, char **line)
{
	static char	*all[ALL_FD];
	char		buffer[BUFF_SIZE + 1];
	int			num;
	char		*tmp;

	if (BUFF_SIZE < 1 || fd < 0 || fd > ALL_FD)
		return (-1);
	num = BUFF_SIZE;
	while (findn(all[fd]) < 0 && num == BUFF_SIZE)
	{
		num = read(fd, buffer, BUFF_SIZE);
		if (num < 0)
			return (-1);
		buffer[num] = '\0';
		tmp = all[fd];
		all[fd] = !(all[fd]) ? ft_strdup(buffer) : ft_strjoin(all[fd], buffer);
		free(tmp);
	}
	if (*all[fd])
		return (!(*line = dup_all(&all[fd])) ? -1 : 1);
	if (!(*line))
		*line = ft_strdup("");
	return (0);
}
