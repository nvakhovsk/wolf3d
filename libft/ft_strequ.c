/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 08:08:31 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 15:56:41 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strequ(char const *s1, char const *s2)
{
	unsigned int	i;

	if ((!(s1)) || (!(s2)))
		return (0);
	i = 0;
	while (s1[i])
	{
		if ((unsigned char)s1[i] - (unsigned char)s2[i])
			return (0);
		i++;
	}
	if (s2[i])
		return (0);
	return (1);
}
