# Wolf3D #
## Wolf3D is a program developped for the exploration of raycasting. Several options are available (see Shortcuts section for more info) ##
* Move within the scene
## **Install & launch** ##
* git clone https://nmatushe@bitbucket.org/nmatushe/wolf3d.git
* cd ~/wolf3d && make && ./wolf3d
## **Shortcuts** ##
* Close the program	 esc 
* Rotate camera	 ◄   ► 
* Move forward/backwards	 ▲   ▼

![Screen Shot 2017-04-29 at 10.56.15 AM.png](https://bitbucket.org/repo/Ggnk5XX/images/1123344448-Screen%20Shot%202017-04-29%20at%2010.56.15%20AM.png)