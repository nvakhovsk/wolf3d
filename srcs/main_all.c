/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_all.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/12 08:06:55 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/14 13:29:00 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		ft_events(t_wolf *wolf)
{
	wolf->player->time_prev_fr = wolf->player->time_cur_fr;
	wolf->player->time_cur_fr = clock();
	wolf->player->move_speed = ((wolf->player->time_cur_fr -
		wolf->player->time_prev_fr) / CLOCKS_PER_SEC) * 3.0;
	wolf->player->rot_speed = ((wolf->player->time_cur_fr -
		wolf->player->time_prev_fr) / CLOCKS_PER_SEC) * 2.0;
	(wolf->rot_left_fl == 1) ? ft_rotation(wolf, 1) : 0;
	(wolf->rot_right_fl == 1) ? ft_rotation(wolf, 0) : 0;
	(wolf->move_for_fl == 1) ? ft_move(wolf, 0) : 0;
	(wolf->move_back_fl == 1) ? ft_move(wolf, 1) : 0;
	ft_redraw(wolf);
	mlx_hook(wolf->in->win, 2, 1, ft_key_press, wolf);
	mlx_hook(wolf->in->win, 3, 2, ft_key_release, wolf);
	mlx_hook(wolf->in->win, 17, 0L, ft_exit, NULL);
	return (0);
}

void	ft_wolf_init(t_wolf *wolf)
{
	t_in		*in;
	t_camera	*camera;

	in = (t_in *)malloc(sizeof(t_in));
	camera = (t_camera *)malloc(sizeof(t_camera));
	wolf->in = in;
	wolf->camera = camera;
	wolf->player->x_position = 2;
	wolf->player->y_position = 2;
	wolf->player->dir_vect_x = 1;
	wolf->player->dir_vect_y = 0;
	wolf->player->cam_plane_x = 0;
	wolf->player->cam_plane_y = 0.6;
	wolf->player->time_cur_fr = 0;
	wolf->player->time_prev_fr = 0;
	wolf->move_for_fl = 0;
	wolf->move_back_fl = 0;
	wolf->rot_left_fl = 0;
	wolf->rot_right_fl = 0;
	wolf->in->init = mlx_init();
	wolf->in->win = mlx_new_window(wolf->in->init, W, H, "wolf3D");
	wolf->in->image = mlx_new_image(wolf->in->init, W, H);
}

void	ft_raycasting(t_wolf *wolf)
{
	int i;

	i = -1;
	while (++i < W)
	{
		ft_ray_camera_init(wolf, i);
		wolf->camera->ray_dir_x < 0 ?
			ft_side_dist(wolf, 0) : ft_side_dist(wolf, 1);
		wolf->camera->ray_dir_y < 0 ?
			ft_side_dist(wolf, 2) : ft_side_dist(wolf, 3);
		ft_map_move(wolf);
		ft_map_draw(wolf, i);
	}
}

void	ft_ariadne(t_wolf *wolf)
{
	ft_wolf_init(wolf);
	ft_read(&(wolf->map), &(wolf->w), &(wolf->h));
	ft_raycasting(wolf);
	mlx_put_image_to_window(wolf->in->init, wolf->in->win, wolf->in->image,
		0, 0);
	mlx_loop_hook(wolf->in->init, ft_events, wolf);
	mlx_loop(wolf->in->init);
}

int		main(void)
{
	t_wolf		*wolf;
	t_player	*player;

	wolf = (t_wolf *)malloc(sizeof(t_wolf));
	player = (t_player *)malloc(sizeof(t_player));
	wolf->player = player;
	ft_ariadne(wolf);
	return (0);
}
