/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pxl.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/14 13:21:54 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/14 13:29:13 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_pxlimg(int x, int y, int rgb, t_in *in)
{
	int				bitspp;
	int				slen;
	int				en;
	char			*image;
	unsigned int	tmp;

	image = mlx_get_data_addr(in->image, &bitspp, &slen, &en);
	tmp = (mlx_get_color_value(in->init, rgb));
	if (y > 0 && y < H && x > 0 && x < W)
		ft_memcpy((void *)(image + slen * y + x * sizeof(int)),
			(void *)&tmp, 4);
}

void	ft_brezenhaim(t_cmap d1, t_cmap d2, int color, t_in *in)
{
	t_brezen	*brzn;

	brzn = (t_brezen *)malloc(sizeof(t_brezen));
	brzn->dx = abs((int)d2.x - (int)d1.x);
	brzn->dy = abs((int)d2.y - (int)d1.y);
	brzn->sx = (d1.x < d2.x) ? 1 : -1;
	brzn->sy = (d1.y < d2.y) ? 1 : -1;
	brzn->err0 = brzn->dx - brzn->dy;
	while ((int)d1.x != (int)d2.x || (int)d1.y != (int)d2.y)
	{
		brzn->err1 = brzn->err0 * 2;
		if (brzn->err1 > -brzn->dy)
		{
			brzn->err0 -= brzn->dy;
			d1.x += brzn->sx;
		}
		if (brzn->err1 < brzn->dx)
		{
			brzn->err0 += brzn->dx;
			d1.y += brzn->sy;
		}
		ft_pxlimg(d1.x, d1.y, color, in);
	}
	free(brzn);
}

void	ft_redraw(t_wolf *wolf)
{
	mlx_destroy_image(wolf->in->init, wolf->in->image);
	wolf->in->image = mlx_new_image(wolf->in->init, W, H);
	ft_raycasting(wolf);
	mlx_put_image_to_window(wolf->in->init, wolf->in->win, wolf->in->image,
		0, 0);
}

int		ft_exit(void *tmp)
{
	(!tmp) ? exit(0) : 0;
	return (0);
}
