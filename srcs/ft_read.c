/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/14 11:19:52 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/14 13:31:03 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		ft_err(void)
{
	ft_putendl("error :)");
	exit(0);
}

void		ft_len_gnl(int *w, int *i)
{
	char	*tmp;
	int		fd;
	int		flag;

	*w = -1;
	(fd = open("map", O_RDONLY)) < 0 ? ft_err() : 0;
	while ((flag = get_next_line(fd, &tmp)))
	{
		if (flag > 0)
			*i = ++(*w) ? *i : ft_strlen(tmp);
		else if (flag < 0)
			return ;
		free(tmp);
	}
	close(fd);
}

void		ft_map_to_arr(int *w, int ***am, int *h, char ***tm)
{
	int		fd;
	int		i;
	int		j;
	int		flag;
	char	*tmp;

	i = -1;
	(fd = open("map", O_RDONLY)) < 0 ? ft_err() : 0;
	*tm = (char **)malloc(sizeof(char *) * (*w + 1));
	while (++i < *w)
		flag = get_next_line(fd, &(*tm)[i]);
	close(fd);
	i = -1;
	*am = (int **)malloc(sizeof(int *) * *w);
	while (++i < *w)
	{
		j = -1;
		(*am)[i] = (int *)malloc(sizeof(int) * *h);
		while (++j < *h)
		{
			tmp = ft_strsub((*tm)[i], j, 1);
			(*am)[i][j] = ft_atoi(tmp);
			free(tmp);
		}
	}
}

void		ft_read(int ***am, int *w, int *h)
{
	char	**tm;
	int		i;

	ft_len_gnl(w, h);
	*w = *w + 1;
	ft_map_to_arr(w, am, h, &tm);
	i = -1;
	while (++i < *w)
		free((tm)[i]);
	free(tm);
}
