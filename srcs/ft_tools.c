/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tools.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/14 13:11:32 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/14 13:26:32 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_rotation(t_wolf *wolf, int flag)
{
	double	dir_x;
	double	rot_speed;
	double	planex;

	dir_x = wolf->player->dir_vect_x;
	rot_speed = (flag == 0) ? wolf->player->rot_speed :
		-wolf->player->rot_speed;
	wolf->player->dir_vect_x = wolf->player->dir_vect_x * cos(rot_speed)
		- wolf->player->dir_vect_y * sin(rot_speed);
	wolf->player->dir_vect_y = dir_x * sin(rot_speed) + wolf->player->dir_vect_y
		* cos(rot_speed);
	planex = wolf->player->cam_plane_x;
	wolf->player->cam_plane_x = wolf->player->cam_plane_x * cos(rot_speed) -
		wolf->player->cam_plane_y * sin(rot_speed);
	wolf->player->cam_plane_y = planex * sin(rot_speed) +
		wolf->player->cam_plane_y * cos(rot_speed);
}

void	ft_inif(t_wolf *wolf)
{
	if (!((wolf->map)[(int)(wolf->player->x_position -
			wolf->player->dir_vect_x * wolf->player->move_speed
			- 0.3 * wolf->player->dir_vect_x)][(int)wolf->player->y_position]))
		wolf->player->x_position -= wolf->player->dir_vect_x *
			wolf->player->move_speed;
}

void	ft_move(t_wolf *wolf, int flag)
{
	if (flag == 0)
	{
		if (!((wolf->map)[(int)(wolf->player->x_position +
			wolf->player->dir_vect_x * wolf->player->move_speed
			+ 0.3 * wolf->player->dir_vect_x)][(int)wolf->player->y_position]))
			wolf->player->x_position += wolf->player->dir_vect_x *
			wolf->player->move_speed;
		if (!((wolf->map)[(int)wolf->player->x_position]
			[(int)(wolf->player->y_position + wolf->player->dir_vect_y *
				wolf->player->move_speed + 0.3 * wolf->player->dir_vect_y)]))
			wolf->player->y_position += wolf->player->dir_vect_y *
				wolf->player->move_speed;
	}
	else
	{
		ft_inif(wolf);
		if (!((wolf->map)[(int)wolf->player->x_position]
			[(int)(wolf->player->y_position - wolf->player->dir_vect_y *
				wolf->player->move_speed - 0.3 * wolf->player->dir_vect_y)]))
			wolf->player->y_position -= wolf->player->dir_vect_y *
				wolf->player->move_speed;
	}
}

int		ft_key_press(int key, t_wolf *wolf)
{
	(key == 53) ? ft_exit(NULL) : 0;
	(key == 125) ? wolf->move_back_fl = 1 : 0;
	(key == 126) ? wolf->move_for_fl = 1 : 0;
	(key == 123) ? wolf->rot_left_fl = 1 : 0;
	(key == 124) ? wolf->rot_right_fl = 1 : 0;
	return (0);
}

int		ft_key_release(int key, t_wolf *wolf)
{
	(key == 125) ? wolf->move_back_fl = 0 : 0;
	(key == 126) ? wolf->move_for_fl = 0 : 0;
	(key == 123) ? wolf->rot_left_fl = 0 : 0;
	(key == 124) ? wolf->rot_right_fl = 0 : 0;
	return (0);
}
