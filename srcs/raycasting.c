/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycasting.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/14 11:19:53 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/14 13:56:45 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		ft_ray_camera_init(t_wolf *wolf, int i)
{
	wolf->camera->cam_coord_x = 2 * i / (double)W - 1;
	wolf->camera->ray_pos_x = wolf->player->x_position;
	wolf->camera->ray_pos_y = wolf->player->y_position;
	wolf->camera->ray_dir_x = wolf->player->dir_vect_x +
		wolf->player->cam_plane_x * wolf->camera->cam_coord_x;
	wolf->camera->ray_dir_y = wolf->player->dir_vect_y +
		wolf->player->cam_plane_y * wolf->camera->cam_coord_x;
	wolf->mapx = (int)wolf->camera->ray_pos_x;
	wolf->mapy = (int)wolf->camera->ray_pos_y;
	wolf->delta_dist_x = sqrt(1 + (wolf->camera->ray_dir_y *
		wolf->camera->ray_dir_y) /
			(wolf->camera->ray_dir_x * wolf->camera->ray_dir_x));
	wolf->delta_dist_y = sqrt(1 + (wolf->camera->ray_dir_x *
		wolf->camera->ray_dir_x) /
			(wolf->camera->ray_dir_y * wolf->camera->ray_dir_y));
}

void		ft_side_dist(t_wolf *wolf, int flag)
{
	if (flag == 0 || flag == 1)
	{
		wolf->xstep = (flag == 0 ? -1 : 1);
		wolf->side_dist_x = (flag == 0 ?
			(wolf->camera->ray_pos_x - wolf->mapx) * wolf->delta_dist_x :
			(wolf->mapx + 1.0 - wolf->camera->ray_pos_x) * wolf->delta_dist_x);
	}
	else
	{
		wolf->ystep = (flag == 2 ? -1 : 1);
		wolf->side_dist_y = (flag == 2 ?
			(wolf->camera->ray_pos_y - wolf->mapy) * wolf->delta_dist_y :
			(wolf->mapy + 1.0 - wolf->camera->ray_pos_y) * wolf->delta_dist_y);
	}
}

void		ft_map_move(t_wolf *wolf)
{
	wolf->dda_flag = 0;
	while (wolf->dda_flag == 0)
	{
		if (wolf->side_dist_x < wolf->side_dist_y)
		{
			wolf->side_dist_x += wolf->delta_dist_x;
			wolf->mapx += wolf->xstep;
			wolf->side = 0;
		}
		else
		{
			wolf->side_dist_y += wolf->delta_dist_y;
			wolf->mapy += wolf->ystep;
			wolf->side = 1;
		}
		if ((wolf->map)[wolf->mapx][wolf->mapy] > 0)
			wolf->dda_flag = 1;
	}
}

void		ft_draw_color(t_wolf *wolf, int i)
{
	t_cmap	cmap1;
	t_cmap	cmap2;

	if (wolf->side)
		wolf->color = (wolf->ystep < 0) ? 0x4a2f48 : 0x748290;
	else
		wolf->color = (wolf->xstep < 0) ? 0x424049 : 0x626262;
	cmap1.x = i;
	cmap1.y = wolf->draw_start;
	cmap2.x = i;
	cmap2.y = wolf->draw_stop;
	ft_brezenhaim(cmap1, cmap2, wolf->color, wolf->in);
	wolf->color = 0x2d130c;
	cmap1.y = wolf->draw_stop + 1;
	cmap2.y = H;
	ft_brezenhaim(cmap1, cmap2, wolf->color, wolf->in);
	wolf->color = 0x906f61;
	cmap1.y = wolf->draw_start;
	cmap2.y = 0;
	ft_brezenhaim(cmap1, cmap2, wolf->color, wolf->in);
}

void		ft_map_draw(t_wolf *wolf, int i)
{
	wolf->dist = (wolf->side == 0) ? (wolf->mapx - wolf->camera->ray_pos_x +
		(1 - wolf->xstep) / 2) / wolf->camera->ray_dir_x :
			(wolf->mapy - wolf->camera->ray_pos_y +
				(1 - wolf->ystep) / 2) / wolf->camera->ray_dir_y;
	wolf->line_heigh = (int)(H / wolf->dist);
	wolf->draw_start = -(wolf->line_heigh) / 2 + H / 2;
	if (wolf->draw_start < 0)
		wolf->draw_start = 0;
	wolf->draw_stop = wolf->line_heigh / 2 + H / 2;
	if (wolf->draw_stop >= H)
		wolf->draw_stop = H - 1;
	ft_draw_color(wolf, i);
}
